package hiker

import "strings"

func FindAnagrams(word string) []string {
	result := []string{word}

	if len(word) > 1 {
		buffer := strings.Split(word, "")
		result = append(result, buffer[1]+buffer[0])
	}
	return result
}
