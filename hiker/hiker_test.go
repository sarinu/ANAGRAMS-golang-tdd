package hiker_test

import (
	"anagrams/hiker"
	"reflect"
	"testing"
)

func TestReturnArrayOfaWhenInputIsa(t *testing.T) {
	result := hiker.FindAnagrams("a")

	expectedResult := []string{"a"}

	if !reflect.DeepEqual(result, expectedResult) {
		t.Errorf("'%+v' should be '%+v'", result, expectedResult)
	}
}

func TestReturnArrayOfbWhenInputIsb(t *testing.T) {
	result := hiker.FindAnagrams("b")

	expectedResult := []string{"b"}

	if !reflect.DeepEqual(result, expectedResult) {
		t.Errorf("'%+v' should be '%+v'", result, expectedResult)
	}
}

func TestReturnArrayOfokAndkoWhenInputIsok(t *testing.T) {
	result := hiker.FindAnagrams("ok")

	expectedResult := []string{"ok", "ko"}

	if !reflect.DeepEqual(result, expectedResult) {
		t.Errorf("'%+v' should be '%+v'", result, expectedResult)
	}
}
